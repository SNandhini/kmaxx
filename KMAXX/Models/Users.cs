﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KMAXX.Models
{
    public class Users
    {
        public int id { set; get; }

        [Required(ErrorMessage = "Provide User Name")]
        [Display(Name = "User Name")]
        public string username { get; set; }
        [Required(ErrorMessage = "Enter Password")]
        [Display(Name = "Password")]
        public string password { get; set; }
        public string roles { get; set; }
        public string phonenumber { get; set; }
        public string validUser { get; set; }
        public DateTime signeddate { get; set; }
    }
    public class newUser
    {

        public int id { set; get; }
        [RegularExpression(@"^[ A-Za-z0-9\[\]()*\-+/%]*$",ErrorMessage ="Enter Valid name")]
        [MaxLength(15,ErrorMessage ="Max Length is 15 chars")]
        [Display(Name = "User Name")]
        [Required(ErrorMessage = "Provide User Name")]
        [Remote("CheckForDuplication", "Home")]
        public string userName { set; get; }
        [Required(ErrorMessage = "Type Password")]
        [RegularExpression(@"^[ A-Za-z0-9\[\]()*\-+/%@]*$", ErrorMessage = "Enter Valid password")]
        [MinLength(6,ErrorMessage ="Mininmum Length of Password should be 6")]
        [Display(Name = "Password")]

        public string password { set; get; }
        [Display(Name ="Confirm Password")]
        [Required(ErrorMessage = "Confirmation Password is required.")]
        [System.ComponentModel.DataAnnotations.Compare("password", ErrorMessage = "Password and Confirmation Password must match.")]
        public string ConfirmPassword { get; set; }
        [Display(Name = "Phone Number")]
        [Required(ErrorMessage = "Provide Phone Number")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Entered phone format is not valid.")]
        public string phoneNumer { set; get; }
        [Display(Name = "Email ID")]
        [Required(ErrorMessage = "Provide EmailId")]
        [EmailAddress(ErrorMessage = "Provide valid Email ID")]
       [MaxLength(75,ErrorMessage = "Provide valid Email ID")]
        public string email { set; get; }
        public string roles { get; set; }
        [Display(Name = " Qualification")]
        [Required(ErrorMessage = "Select Qualification")]
        public string qualification { get; set; }
    }
    public class forgetusers {
        [Required(ErrorMessage ="Enter User Name")]
        [Display(Name = "User Name")]
        public string username { set; get; }
        [Required(ErrorMessage = "Type Password")]
        [MinLength(6, ErrorMessage = "Mininmum Length of Password should be 6")]
        [Display(Name = "Password")]

        public string password { set; get; }
        [Display(Name = "Confirm Password")]
        [Required(ErrorMessage = "Confirmation Password is required.")]
        [System.ComponentModel.DataAnnotations.Compare("password", ErrorMessage = "Password and Confirmation Password must match.")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Email ID")]
        [Required(ErrorMessage = "Enter Mail ID")]
        [EmailAddress(ErrorMessage = "Enter Valid Email Address")]
        public string emailID { set; get; }


    }

}