﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KMAXX.Controllers;
using System.ComponentModel.DataAnnotations;

namespace KMAXX.Models
{
    public class testList
    {
      
        [Required(ErrorMessage ="Enter Subject")]
        public string testSub { get; set; }
        [Required (ErrorMessage = "Enter Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime testDate { get; set; }
        
    }
    public class test
    {
        /// <summary>  
        /// To hold list of orders  
        /// </summary>  
        public List<testList> testDetails { get; set; }

    }
}