﻿using KMAXX.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KMAXX.Models
{
    public class MyViewModel
    {
        [Attachment]
        [DisplayName("Select File to Upload")]
        [Required(ErrorMessage ="Please upload file")]
        //[RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.pdf)$", ErrorMessage = "Entered phone format is not valid.")]
        
        public HttpPostedFileBase File { get; set; }
       
    }
    public class FileUploadDBModel
    {
        public int Id { get; set; }
        public string FileName { get; set; }

        public string Type { get; set; }
        public byte[] File { get; set; }
        public string exam { get; set; }
        public DateTime lastupdated { get; set; }
    }
    public class quspaperfileupload
    {
        public int id { set; get; }
        public string fileName { set; get; }
        public string type { set; get; }
        public byte[] File { get; set; }
        public string exam { get; set; }
    }
    public class notification
    {
        [Required(ErrorMessage = "Provide URL")]
        public string link { get; set; }
        [Required(ErrorMessage = "Enter Display Text")]
        public string text { get; set; }
        public DateTime date { get; set; }
    }
    public class footerenquiry
    {
        [Required(ErrorMessage = "Provide Name")]
        public string name { set; get; }

        [Required(ErrorMessage = "Provide Phone Number")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Entered phone format is not valid.")]
        public string phonenumer { set; get; }

        [Required(ErrorMessage = "Provide EmailId")]
        [EmailAddress(ErrorMessage = "Provide valid Email ID")]
        public string mailid { set; get; }
        [Required(ErrorMessage = " Provide Comments")]
        [MaxLength(200, ErrorMessage = "Max 200 Characters")]
        public string description { set; get; }

    }
    public class batchDetails
    {
        public int id { set; get; }
        public string course { get; set; }
        public string session { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime startsOn { get; set; }
    }
    public class batch
    {
        /// <summary>  
        /// To hold list of orders  
        /// </summary>  
        public List<batchDetails> BatchDetails { get; set; }

    }
    public class AcceptedRecords
    {
        public int id { get; set; }
        public string name { get; set; }
        public string mailId { get; set; }
        public string phnumber { get; set; }
        public string passwrd { get; set; }
        public string qualification { get; set; }
        public string validUser { get; set; }
        public bool extra_checked { get; set; }


    }
    public class aRecs
    {
        public List<AcceptedRecords> amList { set; get; }
    }

}