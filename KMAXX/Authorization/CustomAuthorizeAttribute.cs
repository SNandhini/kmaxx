﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace KMAXX.Authorization
{
    public class CustomAuthorizeAttribute : System.Web.Mvc.AuthorizeAttribute
    {
        public string Message { get; set; }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            var result = new ViewResult();
            result.ViewName = "Login.cshtml";        //this can be a property you don't have to hard code it
            result.MasterName = "_Layout.cshtml";    //this can also be a property
            result.ViewBag.Message = this.Message;
            filterContext.Result = result;
        }
    }
}