﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(KMAXX.Startup))]
namespace KMAXX
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
