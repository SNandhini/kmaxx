﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KMAXX.Controllers
{
    public class ArtifactsController : Controller
    {
        // GET: Artifacts
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Artifacts()
        {
            return View();
        }
        public ActionResult QusPapers()
        {
            return View();
        }
        public ActionResult CurrentAffairs()
        {
            foreach (string upload in Request.Files)
            {
                if (Request.Files[upload].FileName != "")
                {
                    string path = AppDomain.CurrentDomain.BaseDirectory + "/App_Data/uploads/";
                    string filename = Path.GetFileName(Request.Files[upload].FileName);
                    Request.Files[upload].SaveAs(Path.Combine(path, filename));
                }
            }
            return View("CurrentAffairs");
        }

       


        public ActionResult Publication()
        {
            return View();
        }
    }
}