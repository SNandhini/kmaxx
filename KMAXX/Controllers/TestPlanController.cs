﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KMAXX.Models;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using System.Web.Services;
using Newtonsoft.Json.Converters;
using System.Globalization;
using System.Configuration;

namespace KMAXX.Controllers
{
    public class TestPlanController : Controller
    {
        // GET: TestPlan
        //[ValidateAntiForgeryToken]
        public ActionResult testdate()
        {
            if (User.IsInRole("admin"))
            {

                ViewBag.admin = true;
                
            }
            List<testList> list = new List<testList>();
            list = getTestList();
            if ( list != null && list.Count > 0)
            {
                ViewBag.getdata = true;
                test obj = new test();
                obj.testDetails = list;
                return View(obj);
            }
            else
            {
                ViewBag.getdata = false;
                return View();
            }
        }
        public List<testList> getTestList()
        {
            List<testList> userlist = new List<testList>();
            test obj = new test();
            DataSet ds = new DataSet();
            try {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["kmaxxConString"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("select * from testlist", con))
                    {
                        con.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(ds);

                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            testList uobj = new testList();
                            uobj.testSub = ds.Tables[0].Rows[i]["subject"].ToString();
                            uobj.testDate = Convert.ToDateTime(ds.Tables[0].Rows[i]["date"]);
                            if (uobj.testDate > DateTime.Now)
                            {
                                userlist.Add(uobj);
                            }
                        }
                        con.Close();
                        userlist = userlist.OrderByDescending(x => x.testDate).ToList();
                        if (userlist.Count > 0 && userlist != null)
                        {
                            return userlist;
                        }
                        else
                        {
                            return null;
                        }

                    }

                }
            }
            catch(Exception) {
                return null;
            }
            
        }
        public ActionResult addtest()
        {

            return View();
        }
        [WebMethod]
        [HttpPost]
        public ActionResult add(List<testList> list)
        {

            return View();
        }
        [WebMethod]
        [HttpPost]
        public ActionResult SaveData(string emp)//WebMethod to Save the data  
        {
            try
            {
                if (emp == null || emp == "[]")
                {

                    return Json(new { success = false, responseText = "Please Check Values" }, JsonRequestBehavior.AllowGet);
                }
                else
                {

                    var json = Request.RequestContext.HttpContext.Request.Params["emp"];
                    var format = "yyyy-mm-dd"; // your datetime format
                                               //var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };
                    var serializeData = JsonConvert.DeserializeObject<List<testList>>(emp);
                    if (serializeData != null && serializeData.Count() > 0)
                    {
                        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["kmaxxConString"].ConnectionString))
                        {
                            foreach (var data in serializeData)
                            {
                                DateTime date = Convert.ToDateTime(DateTime.Parse(data.testDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture)));
                                if (date != null && date >= DateTime.Now)
                                {
                                    using (var cmd = new SqlCommand("INSERT INTO testlist VALUES(@sub,@date)"))
                                    {
                                        cmd.CommandType = CommandType.Text;
                                        cmd.Parameters.AddWithValue("@sub", data.testSub);
                                        cmd.Parameters.AddWithValue("@date", data.testDate);
                                        cmd.Connection = con;
                                        if (con.State == ConnectionState.Closed)
                                        {
                                            con.Open();
                                        }
                                        cmd.ExecuteNonQuery();
                                        con.Close();
                                    }
                                }
                                else
                                {
                                    return Json(new { success = false, responseText = "Date should be today or future" }, JsonRequestBehavior.AllowGet);
                                }

                            }
                        }

                    }
                    else
                    {
                        return Json(new { success = false, responseText = "Please check the date and subject" }, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new { success = true, responseText = "Successfully Updated", Url = "testdate" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                return Json(new { success = false, responseText = "Please Check Values" }, JsonRequestBehavior.AllowGet);
            }
           
        }
        public ActionResult YearPlan()
        {

            return View(); }
    }
    
}