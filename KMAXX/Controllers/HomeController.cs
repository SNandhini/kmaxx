﻿using KMAXX.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Security;

namespace KMAXX.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            ViewBag.notifications = populateNotification();
            if (User.IsInRole("admin"))
            {

                ViewBag.admin = true;
                return View();
            }

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Gallery()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        #region form to register
        public ActionResult CreateLogin(string returnUrl)
        {
            ViewBag.returnUrl = returnUrl;
            ViewBag.listItems = new SelectList(new[]{
    new SelectListItem{ Text = "B.Sc",Value = "B.Sc"},
    new SelectListItem{ Text = "B.A",Value = "B.A"},
    new SelectListItem{ Text="B.Com", Value="B.Com"},
     new SelectListItem{ Text="M.Sc", Value="M.Sc"},
      new SelectListItem{ Text="M.Com", Value="M.Com"},
       new SelectListItem{ Text="B.Lit", Value="B.Lit"},
        new SelectListItem{ Text="SSLC", Value="SSLC"},
         new SelectListItem{ Text="HSc", Value="HSc"},
          new SelectListItem{ Text="Diplomo", Value="Diplomo"},
           new SelectListItem{ Text="M.A", Value="M.A"},
    }, "Value", "Text");
            return View();
        }
        #endregion

        #region to insert user details and send mail to users
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public ActionResult Register(newUser model, string returnUrl)
        {
            bool userValid = false;
            if (ModelState.IsValid)
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["kmaxxConString"].ConnectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("insert into users (username,Pword,phone,roles,mail,qua,validUser,signeddate)" + " values(@name,@pasword,@phno,@roles,@mailId,@qua,@validUser,@updatedate)", con))
                        {
                            con.Open();
                            cmd.CommandType = CommandType.Text;
                            cmd.Parameters.Add("@name", SqlDbType.VarChar).Value = model.userName;
                            cmd.Parameters.Add("@pasword", SqlDbType.VarChar).Value = model.password;
                            cmd.Parameters.Add("@mailId", SqlDbType.VarChar).Value = model.email;
                            cmd.Parameters.Add("@phno", SqlDbType.VarChar).Value = model.phoneNumer;
                            cmd.Parameters.Add("@roles", SqlDbType.VarChar).Value = "user";
                            cmd.Parameters.Add("@qua", SqlDbType.VarChar).Value = model.qualification;
                            cmd.Parameters.Add("@validUser", SqlDbType.VarChar).Value = "f";
                            cmd.Parameters.Add("@updatedate", SqlDbType.DateTime).Value = DateTime.Now;
                            int rowsaff= cmd.ExecuteNonQuery();

                            bool sent = sendapprovaltoadmin(model);
                            if (sent)
                            { ViewBag.status = "Successfully Registered.Please wait. Your application is being processed by admin."; }
                            else { }
                            ViewBag.status = " We have saved your application.Please wait till it gets approved";

                        }
                        con.Close();
                    }
                }
                catch(Exception)
                {
                    ViewBag.status = "Failed. Please try again later";
                    return View();
                }
            }
            ViewBag.returnUrl = returnUrl;
          
            return View();
        }
        #endregion
        #region send mail to admin
        public bool sendapprovaltoadmin(newUser model)
        {
            try
            {
                using (MailMessage mailMessage = new MailMessage())
                {
                    mailMessage.From = new MailAddress(ConfigurationManager.AppSettings["UserName"]);
                    mailMessage.Subject = "Approval for " + model.userName;
                    mailMessage.Body = "Approval Needed for below user :" + "<br/>Name :" + model.userName + " <br/><a target = '_blank' href = 'http://www.k-maxxtnpsckanchi.com/Login/Login' > Click here to Provide approval</a ><br/><br/> Thanks,<br/> " + model.userName;
                    //mailMessage.Body = "Approval Needed for below user :" + "<br/>Name :" + model.userName + "<br/>Phone Number :" + model.phoneNumer + "<br/>Mail ID :" + model.email + "<br/>Qualification : " + model.qualification + "<br/><a target='_blank' href='http://www.k-maxxtnpsckanchi.com/Login/Login'>Click here to Provide approval</a><br/><br/>Thanks,<br/>" + model.userName;
                    mailMessage.IsBodyHtml = true;
                    mailMessage.To.Add(new MailAddress("kmaxxtnpsckanchi@gmail.com"));
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = ConfigurationManager.AppSettings["Host"];
                    smtp.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
                    System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                    NetworkCred.UserName = ConfigurationManager.AppSettings["UserName"];
                    NetworkCred.Password = ConfigurationManager.AppSettings["Password"];
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;

                    smtp.Port = int.Parse(ConfigurationManager.AppSettings["Port"]);
                    smtp.Send(mailMessage);
                }
            }
            catch (Exception)
            {
                ViewBag.status = " We have saved your application. Please wait till it gets approved";

            }

            return true; }
        #endregion

        #region insert notification details by admin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult getthelatestnotification(notification model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["kmaxxConString"].ConnectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("insert into notifications values(@displaytext, @link,@date)", con))
                        {
                            con.Open();
                            cmd.Parameters.Add("@displaytext", SqlDbType.VarChar).Value = model.text;
                            cmd.Parameters.Add("@link", SqlDbType.VarChar).Value = model.link;
                            cmd.Parameters.Add("@date", SqlDbType.Date).Value = DateTime.Now;
                            cmd.ExecuteNonQuery();

                        }
                        con.Close();
                    }
                }
                catch(Exception) {
                    return RedirectToAction("Index");
                }
               
            }
            return RedirectToAction("Index");
        }

        #endregion

        #region get notification from database
        public List<notification> populateNotification()
        {
            DataSet ds = new DataSet();
            List<notification> obj = new List<notification>();
            try {

                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["kmaxxConString"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("select * from notifications", con))
                    {
                        con.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(ds);
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            notification uobj = new notification();
                            uobj.text = ds.Tables[0].Rows[i]["displaytext"].ToString();
                            uobj.link = ds.Tables[0].Rows[i]["link"].ToString();
                            uobj.date = Convert.ToDateTime(ds.Tables[0].Rows[i]["date"]);
                            obj.Add(uobj);
                        }
                        obj = obj.OrderByDescending(x => x.date).Take(5).ToList();
                    }
                    con.Close();
                }
            }
            catch (Exception) {
                return null;
            }
            
            return obj;
        }

        #endregion

        #region to list all the notification
        public ActionResult AllNoti()
        {
            DataSet ds = new DataSet();
            List<notification> obj = new List<notification>();
            try {

                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["kmaxxConString"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("select * from notifications", con))
                    {
                        con.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(ds);
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            notification uobj = new notification();
                            uobj.text = ds.Tables[0].Rows[i]["displaytext"].ToString();
                            uobj.link = ds.Tables[0].Rows[i]["link"].ToString();
                            uobj.date = Convert.ToDateTime(ds.Tables[0].Rows[i]["date"]);
                            obj.Add(uobj);
                        }
                        obj = obj.OrderByDescending(x => x.date).ToList();
                        ViewBag.allnotify = obj;
                    }
                    con.Close();
                }
            }
            catch (Exception) {
                return View("error");
            }
           
            return View();
        }
        #endregion
        public ActionResult footer()
        {

            return View();
        }

        #region send query to admin mail
        public ActionResult sendqueries(footerenquiry model)
        {
            bool sent = sendmail(model);
            if (sent == true)
            { ViewBag.status = "Thanks for your message. We will get back to you soon"; }
            else ViewBag.status = "error";
            //  }
            //catch (Exception)
            //{
            //    ViewBag.status = "Message sent failed. Please send again later";
            //}

            return RedirectToAction("Index");
        }
        #endregion
        public ActionResult RemoteValidation(newUser user)
        {
            return View(user);
        }

        #region check user name validation
        public JsonResult CheckForDuplication(string username)
        {
            bool isExist = false;
            DataSet ds = new DataSet();
            List<newUser> obj = new List<newUser>();
            try
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["kmaxxConString"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("select username from users", con))
                    {
                        con.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(ds);
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            newUser uobj = new newUser();
                            uobj.userName = ds.Tables[0].Rows[i]["username"].ToString();

                            obj.Add(uobj);
                        }

                    }
                    con.Close();
                    isExist = obj.Exists(x => x.userName.ToUpper().Equals(username.ToUpper()));
                }
                if (isExist)
                {
                    return Json("Sorry, this name already exists", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                return Json("Sorry, something went wrong.Please try again later", JsonRequestBehavior.AllowGet);
            }
           
        }

        #endregion
        public ActionResult forgetPassword()
        {

            return View();
        }

        #region change user password
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult changePassword(forgetusers model)
        {
            forgetusers obj = new forgetusers();
            DataSet ds = new DataSet();
            if (model.ConfirmPassword != null && model.password != null && model.username != null)
            {
                SqlConnection connection = new SqlConnection();
                try
                {
                    using (connection = new SqlConnection(ConfigurationManager.ConnectionStrings["kmaxxConString"].ConnectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("select username from users where username = @username", connection))
                        {
                            connection.Open();
                            cmd.Parameters.AddWithValue("@username", model.username);
                            SqlDataAdapter da = new SqlDataAdapter(cmd);
                            da.Fill(ds);
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                obj.username = ds.Tables[0].Rows[i]["username"].ToString();
                            }
                        }
                        if (obj.username != null && obj.username != "")
                        {
                            using (SqlCommand cmd = new SqlCommand("update users set Pword = @password where username=@username", connection))
                            {
                                cmd.Parameters.Add("@password", SqlDbType.VarChar).Value = model.password;
                                cmd.Parameters.Add("@username", SqlDbType.VarChar).Value = model.username;
                                cmd.ExecuteNonQuery();
                            }
                        }
                        else
                        {
                            ViewBag.status = "User Name does not exist";
                            return View("forgetPassword");
                        }
                    }
                }
                catch (Exception)
                {
                    ViewBag.status = "Error Occured in Server";
                    return View("forgetPassword");
                }
                finally { connection.Close(); }
            }
            return RedirectToAction("Login", "Login");
        }

        #endregion
        public ActionResult forgetUserName()
        {
            return View();
        }
        #region sending username to user
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult getUsername(forgetusers model)
        {

            forgetusers obj = new forgetusers();
            DataSet ds = new DataSet();
            if (model.emailID != null && model.emailID != "")
            {
                SqlConnection connection = new SqlConnection();
                try
                {
                    using (connection = new SqlConnection(ConfigurationManager.ConnectionStrings["kmaxxConString"].ConnectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("select * from users where mail = @mail", connection))
                        {
                            connection.Open();
                            cmd.Parameters.AddWithValue("@mail", model.emailID);
                            SqlDataAdapter da = new SqlDataAdapter(cmd);
                            da.Fill(ds);
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                obj.username = ds.Tables[0].Rows[i]["username"].ToString();
                            }
                        }
                        if (obj.username != null && obj.username != "")
                        {
                            try
                            {

                                using (MailMessage mailMessage = new MailMessage())
                                {
                                    mailMessage.From = new MailAddress(ConfigurationManager.AppSettings["UserName"]);
                                    mailMessage.Subject = "Forget Username";
                               
                                    mailMessage.Body = "Hi " + obj.username + ", <br/>Kindly find below your user name <br/>User Name: " + obj.username + "<br/>Please <a target='_blank'  href='http://www.k-maxxtnpsckanchi.com/Login/Login'>login</a> with this user name<br/> Thanks,<br/>K-Maxx";
                                    mailMessage.IsBodyHtml = true;
                                    mailMessage.To.Add(new MailAddress(model.emailID));
                                    SmtpClient smtp = new SmtpClient();
                                    smtp.Host = ConfigurationManager.AppSettings["Host"];
                                    smtp.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
                                    System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                                    NetworkCred.UserName = ConfigurationManager.AppSettings["UserName"];
                                    NetworkCred.Password = ConfigurationManager.AppSettings["Password"];
                                    smtp.UseDefaultCredentials = true;
                                    smtp.Credentials = NetworkCred;

                                    smtp.Port = int.Parse(ConfigurationManager.AppSettings["Port"]);
                                    smtp.Send(mailMessage);
                                }
                                //WebMail.SmtpServer = "smtp.gmail.com";
                                //WebMail.SmtpPort = 587;
                                //WebMail.SmtpUseDefaultCredentials = true;
                                //WebMail.EnableSsl = true;
                                //WebMail.UserName = "kmaxxtnpsckanchi@gmail.com";
                                //WebMail.Password = "kanchipuram";
                                //WebMail.From = model.emailID;
                                //var body = "Hi " + obj.username + ", <br/>Kindly find below your user name <br/>User Name: " + obj.username + "<br/>Please login with this user name<br/> Thanks,<br/>K-Maxx";
                                //WebMail.Send(to: model.emailID, subject: "Forget Username ", body: body);
                                ViewBag.status = "Your username has been sent to your mail. Please check and login";

                            }
                            catch (Exception)
                            {
                                ViewBag.status = "Failed. Please send again later";
                            }
                        }
                        else
                        {
                            ViewBag.status = "Please check the Email Address";
                            return View("forgetUserName");
                        }
                    }
                }
                catch (Exception)
                {
                    ViewBag.status = "Error Occured in Server";
                    return View("forgetUserName");
                }
                finally { connection.Close(); }
            }
            return View("forgetUserName");



        }
        #endregion

        #region get userdetails from db for admin approval 
        public ActionResult Approve()
        {
            SqlConnection connection = new SqlConnection();
            DataSet ds = new DataSet();
            aRecs ubj = new aRecs();
            List<AcceptedRecords> uobj = new List<AcceptedRecords>();
            
            try
            {
                using (connection = new SqlConnection(ConfigurationManager.ConnectionStrings["kmaxxConString"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("select * from users where validUser=@validuser", connection))
                    {
                        connection.Open();

                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        cmd.Parameters.AddWithValue("@validuser", "f");
                        da.Fill(ds);
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            AcceptedRecords obj = new AcceptedRecords();
                            obj.id = Convert.ToInt16((ds.Tables[0].Rows[i]["id"]));
                                obj.name = (ds.Tables[0].Rows[i]["username"]).ToString();
                                obj.mailId = (ds.Tables[0].Rows[i]["mail"]).ToString();
                                obj.phnumber = (ds.Tables[0].Rows[i]["phone"]).ToString();
                                obj.qualification = (ds.Tables[0].Rows[i]["qua"]).ToString();
                                obj.passwrd = (ds.Tables[0].Rows[i]["Pword"]).ToString();
                                uobj.Add(obj);
                            
                           
                        }
                        ubj.amList = uobj;
                        if (ubj.amList.Count() == 0)
                        { ViewBag.user = "No record found"; }
                    }
                }
                return View(ubj);
            }
            catch (Exception)
            { ViewBag.user = "No record found"; }
            return View(ubj);
        }
        #endregion

        #region admin approval update

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult updateregister(aRecs model)
        {
            
             
           var accepted = new List<string>();
           var status = new List<string>();
            var name = new List<string>();
            if (ModelState.IsValid)
            {
                //try {
                    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["kmaxxConString"].ConnectionString))
                    {
                        DataTable dt = new DataTable();
                        dt.Columns.Add("id");
                        dt.Columns.Add("checked");

                        for (int i = 0; i < model.amList.Count(); i++)
                        {
                            if (model.amList[i].extra_checked == true)
                            {
                                int id = model.amList[i].id;
                                string check = "t";
                                dt.Rows.Add(id, check);
                               accepted.Add(model.amList[i].mailId);
                            status.Add("Accepted");
                            name.Add(model.amList[i].name);
                               
                            }
                            else if (model.amList[i].extra_checked == false)
                            {
                                int id = model.amList[i].id;
                                string check = "r";
                                dt.Rows.Add(id, check);
                              
                            accepted.Add(model.amList[i].mailId);
                            status.Add("Rejected");
                            name.Add(model.amList[i].name);
                        }
                            
                        }
                        using (SqlCommand cmd = new SqlCommand("updateUsers", con))
                        {
                            con.Open();
                            cmd.CommandText = "updateUsers";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@DetailUpdate", dt);
                            cmd.ExecuteNonQuery();

                        }
                        con.Close();
                       bool sent= sendstatus(accepted,status,name);
                        if (sent)
                        { ViewBag.success = "Approved Successfully."; }
                        else { ViewBag.success = "Error!"; }
                        
                    }
                //}
                //catch (Exception)
                //{
                //    ViewBag.success = "Error!";
                //}  
            }
            
            return View();
        }
        #endregion

        #region sending approved status to user
        public bool sendstatus(List<string> mails,List<string> status,List<string> name )
        {
            for (int i=0;i<mails.Count();i++)
            {
                try {
                    using (MailMessage mailMessage = new MailMessage())
                    {

                        mailMessage.From = new MailAddress(ConfigurationManager.AppSettings["UserName"]);
                        mailMessage.Subject = "K-maxx - Request status";

                        mailMessage.Body = "Hi " + name[i] + ", <br/> Your request to <a target='_blank'  href='http://www.k-maxxtnpsckanchi.com/Login/Login'> K-Maxx </a> has been " + status[i] + " .</br> <br/><br/> Thanks,<br/>K-Maxx";
                        mailMessage.IsBodyHtml = true;
                        mailMessage.To.Add(new MailAddress(mails[i]));
                        SmtpClient smtp = new SmtpClient();
                        smtp.Host = ConfigurationManager.AppSettings["Host"];
                        smtp.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
                        System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                        NetworkCred.UserName = ConfigurationManager.AppSettings["UserName"];
                        NetworkCred.Password = ConfigurationManager.AppSettings["Password"];
                        smtp.UseDefaultCredentials = true;
                        smtp.Credentials = NetworkCred;

                        smtp.Port = int.Parse(ConfigurationManager.AppSettings["Port"]);
                        smtp.Send(mailMessage);
                    }
                }
                catch (Exception) {
                    return false;
                }
                
            }
            return true;
        }
        #endregion

        #region send mail to admin 
        public bool sendmail(footerenquiry model)
        {
            using (MailMessage mailMessage = new MailMessage())
            {
                mailMessage.From = new MailAddress(ConfigurationManager.AppSettings["UserName"]);
                mailMessage.Subject = "Info for " + model.name;
                mailMessage.Body = "From :" + model.name + "<br/>Phone Number :" + model.phonenumer + "<br/>Mail ID :" + model.mailid + "<br/>" + model.description + "<br/><br/>Thanks,<br/>"+model.name;
                mailMessage.IsBodyHtml = true;
                mailMessage.To.Add(new MailAddress("kmaxxtnpsckanchi@gmail.com"));
                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["Host"];
                smtp.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
                System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                NetworkCred.UserName = ConfigurationManager.AppSettings["UserName"];
                NetworkCred.Password = ConfigurationManager.AppSettings["Password"];
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;

                smtp.Port = int.Parse(ConfigurationManager.AppSettings["Port"]);
                smtp.Send(mailMessage);
            }
            return true;


        }
        #endregion

        #region display all the users
        public ActionResult allusers()
        {
            SqlConnection connection = new SqlConnection();
            DataSet ds = new DataSet();
            aRecs ubj = new aRecs();
            List<AcceptedRecords> uobj = new List<AcceptedRecords>();

            try
            {
                using (connection = new SqlConnection(ConfigurationManager.ConnectionStrings["kmaxxConString"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("select * from users", connection))
                    {
                        connection.Open();

                        SqlDataAdapter da = new SqlDataAdapter(cmd);

                        da.Fill(ds);
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            AcceptedRecords obj = new AcceptedRecords();
                            obj.id = Convert.ToInt16((ds.Tables[0].Rows[i]["id"]));
                            obj.name = (ds.Tables[0].Rows[i]["username"]).ToString();
                            obj.mailId = (ds.Tables[0].Rows[i]["mail"]).ToString();
                            obj.phnumber = (ds.Tables[0].Rows[i]["phone"]).ToString();
                            obj.qualification = (ds.Tables[0].Rows[i]["qua"]).ToString();
                            obj.validUser = (ds.Tables[0].Rows[i]["validUser"]).ToString();

                            uobj.Add(obj);


                        }
                        ubj.amList = uobj;
                        if (ubj.amList.Count == 0)
                        { ViewBag.user = "No record found "; }
                    }
                }
                return View(ubj);
            }
            catch (Exception)
            { ViewBag.user = "Error please try again "; }

            return View(ubj);
        }
        #endregion
    }

}