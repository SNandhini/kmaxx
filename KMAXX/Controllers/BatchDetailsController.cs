﻿using KMAXX.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;

namespace KMAXX.Controllers
{
    public class BatchDetailsController : Controller
    {
        // GET: BatchDetails
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult updateDetails()
        {
            if (User.IsInRole("admin"))
            {

                ViewBag.admin = true;

            }
            List<batchDetails> list = new List<batchDetails>();
            list = getBatchDetails();
            if (list != null && list.Count > 0)
            {
                ViewBag.get = true;
                batch obj = new batch();
                obj.BatchDetails = list;
                return View(obj);
            }
            else
            {
                ViewBag.get = false;
                //ModelState.AddModelError("testSub", "Subject not found");
                //ModelState.AddModelError("testDate", "Date not found ");
                return View();
            }
       
        }
        public List<batchDetails> getBatchDetails()
        {
            List<batchDetails> userlist = new List<batchDetails>();
            test obj = new test();
            DataSet ds = new DataSet();
            try {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["kmaxxConString"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("select * from batchDetails", con))
                    {
                        con.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(ds);

                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            batchDetails uobj = new batchDetails();
                            uobj.course = ds.Tables[0].Rows[i]["course"].ToString();
                            uobj.startsOn = Convert.ToDateTime(ds.Tables[0].Rows[i]["startsOn"]);
                            uobj.session = ds.Tables[0].Rows[i]["session"].ToString();

                            userlist.Add(uobj);

                        }
                        con.Close();
                        userlist = userlist.OrderByDescending(x => x.startsOn).ToList();
                        if (userlist.Count > 0 && userlist != null)
                        {
                            return userlist;
                        }
                        else
                        {
                            return null;
                        }

                    }

                }
            }
            catch(Exception)
            {
                return null;
            }
            
        }
        public ActionResult updateBatch(List<batchDetails> model)
        {
            return View();
        }
        [WebMethod]
        [HttpPost]
        public ActionResult SaveData(string emp)//WebMethod to Save the data  
        {
            try {
                if (emp == null || emp == "[]")
                {

                    return Json(new { error = true, responseText = "Please Check Values", Url = "updateDetails" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var json = Request.RequestContext.HttpContext.Request.Params["emp"];
                    var format = "yyyy-mm-dd"; // your datetime format
                                               //var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };

                    List<batchDetails> serializeData = JsonConvert.DeserializeObject<List<batchDetails>>(emp);
                    if (serializeData != null && serializeData.Count() > 0)
                    {
                        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["kmaxxConString"].ConnectionString))
                        {
                            foreach (var data in serializeData)
                            {
                                DateTime date = Convert.ToDateTime(DateTime.Parse(data.startsOn.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture)));

                                if (date >= DateTime.Now)
                                {
                                    using (var cmd = new SqlCommand("INSERT INTO batchdetails VALUES(@course,@startsOn,@session)"))
                                    {
                                        cmd.CommandType = CommandType.Text;
                                        cmd.Parameters.AddWithValue("@course", data.course);
                                        cmd.Parameters.AddWithValue("@startsOn", data.startsOn.ToShortDateString());
                                        cmd.Parameters.AddWithValue("@session", data.session);
                                        cmd.Connection = con;
                                        if (con.State == ConnectionState.Closed)
                                        {
                                            con.Open();
                                        }
                                        cmd.ExecuteNonQuery();
                                        con.Close();
                                    }
                                }
                                else
                                {
                                    return Json(new { success = false, responseText = "Date should be today or future", Url = "updateDetails" }, JsonRequestBehavior.AllowGet);
                                }

                            }
                        }

                    }
                    else
                    {
                        return Json(new { error = true, responseText = "Please check the date and subject", Url = "updateDetails" }, JsonRequestBehavior.AllowGet);
                    }
                    // return Json(new { success = true, responseText = "Please check the date and subject", Url = "updateDetails" });
                    return Json(new { success = true, responseText = "Successfully Updated", Url = "updateDetails" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                return Json(new { error = true, responseText = "Please Check Values", Url = "updateDetails" }, JsonRequestBehavior.AllowGet);
            }
            
        }
    }
}