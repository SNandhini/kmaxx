﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KMAXX.Controllers
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property , AllowMultiple = false,Inherited =true)]
    public sealed class AttachmentAttribute : ValidationAttribute
    { 
        public readonly HttpPostedFile _file;
        public readonly Int32 filesize;
        public readonly string fileext;
       
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            HttpPostedFileBase file = value as HttpPostedFileBase;
            if (file == null)
            {

                return new ValidationResult("Please upload a file");
            }
            if (file.ContentLength > 5 * 1024 * 1024)
            {
                return new ValidationResult("The file size is big");
            }
            string ext = Path.GetExtension(file.FileName);
            if (string.IsNullOrEmpty(ext) || !ext.Equals(".pdf", StringComparison.OrdinalIgnoreCase))
            {
                return new ValidationResult("Not a PDF file");
            }
            return ValidationResult.Success;
        }
       
    }
}