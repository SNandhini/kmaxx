﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KMAXX.Models;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace KMAXX.Controllers
{
    public class currentAffairsController : Controller
    {
        // GET: currentAffairs

        public ActionResult Index(string mode)
        {
            if (User.IsInRole("admin"))
            {
                
                return RedirectToAction("forAdmin",new { mode = mode});
            }
            else if (User.IsInRole("user"))
            {
               // TempData["modes"] = mode;
                return RedirectToAction("Download", new { mode = mode });
            }
            else
            {
                ViewBag.returnUrl = mode;
                return View("loginrequired");
            }
        }


        public ActionResult upload()
        {
            var model = new MyViewModel();
            return View(model);
        }

        #region insert files to db
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult addfile(MyViewModel model, string mode)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.success = "File should be in PDF format and Size should be less than 5 mb";
                return View();
            }
            if (ModelState.IsValid)
            {
                if (model.File != null)
                {
                    FileUploadDBModel fileUploadModel = new FileUploadDBModel();

                    byte[] uploadFile = new byte[model.File.InputStream.Length];
                    model.File.InputStream.Read(uploadFile, 0, uploadFile.Length);

                    fileUploadModel.FileName = model.File.FileName;
                    fileUploadModel.File = uploadFile;
                    // fileUploadModel.exam = model.exam;
                    try
                    {
                        using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["kmaxxConString"].ConnectionString))
                        {
                            using (SqlCommand cmd = new SqlCommand("insert into quspapers values(@name, @type, @data,@exam,@date)", connection))
                            {
                                connection.Open();
                                cmd.Parameters.Add("@name", SqlDbType.VarChar).Value = fileUploadModel.FileName;
                                cmd.Parameters.Add("@type", SqlDbType.VarChar).Value = model.File.ContentType;
                                cmd.Parameters.Add("@data", SqlDbType.Binary).Value = uploadFile;
                                cmd.Parameters.Add("@exam", SqlDbType.VarChar).Value = mode;
                                cmd.Parameters.Add("@date", SqlDbType.DateTime).Value = DateTime.Now;
                                cmd.ExecuteNonQuery();
                            }
                            connection.Close();
                        }
                    }
                    catch (Exception)
                    {
                        ViewBag.sta = "Error in uplading file";
                    }
                   
                }
                else
                {
                    ViewBag.sta = "File not selected";
                }
            }
            //else {
            //    HttpPostedFileBase upload = Request.Files["File"];
            //    if (upload.ContentLength == 0)
            //    {
            //        ModelState.AddModelError("File", "Please upload file");
            //    }
            //    else if (upload.ContentLength > 0)
            //    {
            //        string filename = upload.FileName;
            //        string type = upload.ContentType;
            //        byte[] temp = new byte[upload.ContentLength];
            //        var data = upload.InputStream.Read(temp,0,Convert.ToInt16(upload.ContentLength));
            //        var types = MVCSecurity.Filters
            //    }

            //}
          
            // SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["kmaxxConString"].ConnectionString);
            //query = "insert into PDFFiles (Name,type,data)" + " values (@Name, @type, @Data)";   //insert query
            // SqlCommand com = new SqlCommand("insert into PDFFiles (Name,type,data)" + " values (@Name, @type, @Data)", con);

            return RedirectToAction("Index",new {mode = mode });
        }
        #endregion

        #region download files from db
        public ActionResult Download(string mode)
        {
            // string mode = TempData["modes"].ToString();
            if (mode != null && mode != "")
            {
                if (mode == "qusPaper")
                { ViewBag.st = true; }

                List<FileUploadDBModel> obj = new List<FileUploadDBModel>();
                obj = toGetFiles(mode);
                return View(obj);

            }
            else { return View("error");

            }
        }

        #endregion

        #region to get files from db
        public List<FileUploadDBModel> toGetFiles(string mode)
        {
            List<FileUploadDBModel> obj = new List<FileUploadDBModel>();
            DataSet ds = new DataSet();
            try {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["kmaxxConString"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("select * from quspapers where exam = @mode", con))
                    {
                        con.Open();
                        cmd.Parameters.AddWithValue("@mode", mode);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(ds);
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            FileUploadDBModel uobj = new FileUploadDBModel();
                            uobj.Id = Convert.ToInt16(ds.Tables[0].Rows[i]["id"]);
                            uobj.FileName = ds.Tables[0].Rows[i]["name"].ToString();
                            uobj.Type = ds.Tables[0].Rows[i]["type"].ToString();
                            uobj.File = (byte[])(ds.Tables[0].Rows[i]["data"]);
                            uobj.lastupdated = Convert.ToDateTime(ds.Tables[0].Rows[i]["lastupdated"]);
                            obj.Add(uobj);
                        }
                        // obj.OrderByDescending(x => x.lastupdated);
                        obj = obj.OrderByDescending(x => x.lastupdated).ToList();
                    }
                    con.Close();
                }
            }
            catch(Exception) {
                return null;
            }
           
            return obj;
        }
        #endregion

        #region download file
        public FileContentResult FileDownload(int? id)
        {
            byte[] fileData = { };
            string fileName = "";
            DataSet ds = new DataSet();
            try {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["kmaxxConString"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("select * from quspapers where id=@id", con))
                    {
                        con.Open();
                        cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(ds);

                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            FileUploadDBModel uobj = new FileUploadDBModel();
                            uobj.Id = Convert.ToInt16(ds.Tables[0].Rows[i]["id"]);
                            fileName = ds.Tables[0].Rows[i]["name"].ToString();
                            uobj.Type = ds.Tables[0].Rows[i]["type"].ToString();
                            fileData = (byte[])(ds.Tables[0].Rows[i]["data"]);
                        }
                    }
                    con.Close();

                    return File(fileData, "text", fileName);
                }
            }
            catch(Exception) {
                return File(fileData,"text");
            }
           
        }
        #endregion

        #region admin
        public ActionResult forAdmin(string mode)
        {
            // string mode = TempData["modes"].ToString();
            //List<FileUploadDBModel> fileList = new List<FileUploadDBModel>();
            try {
                ViewBag.files = toGetFiles(mode);
                ViewBag.mode = mode;
            }
            catch(Exception) {
                return View("error");
            }
           
            return View("upload");
        }
        #endregion
        public ActionResult publication()
        {
            return View();
        }
        
    }

}