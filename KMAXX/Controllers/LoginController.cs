﻿using KMAXX.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace KMAXX.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
       
        public ActionResult Login(string returnUrl)
        {
            ViewBag.returnUrl = returnUrl;
            return View();
        }
        #region users login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Users model, string ReturnUrl)
        {
            DataSet ds = new DataSet();
            Users uobj = new Users();
            bool userValid = false;
            // Lets first check if the Model is valid or not
            if (ModelState.IsValid)
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["kmaxxConString"].ConnectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("select * from users where username = @username", con))
                        {
                            cmd.CommandType = CommandType.Text;
                            cmd.Parameters.AddWithValue("@username", model.username);
                            con.Open();

                            SqlDataAdapter da = new SqlDataAdapter(cmd);
                            da.Fill(ds);

                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {

                                uobj.username = ds.Tables[0].Rows[i]["username"].ToString();
                                Session["UserID"] = uobj.username;
                                Session["role"] = uobj.roles;
                                uobj.password = ds.Tables[0].Rows[i]["Pword"].ToString();
                                uobj.validUser = ds.Tables[0].Rows[i]["validUser"].ToString();
                                uobj.signeddate = Convert.ToDateTime(ds.Tables[0].Rows[i]["signeddate"]);
                                // uobj.testDate = Convert.ToDateTime(ds.Tables[0].Rows[i]["date"]);                           
                            }
                        }
                        con.Close();
                    }
                    if (uobj.validUser.ToLower() == "t" || uobj.validUser.ToLower() == "f" && uobj.signeddate.AddDays(2).Date < DateTime.Now.Date)
                    {
                        if (uobj.password == model.password && uobj.username.ToUpper() == model.username.ToUpper())
                        {
                            userValid = true;
                        }
                        else
                        {
                            userValid = false;
                            ModelState.AddModelError("", "The user name or password provided is incorrect.");
                        }
                    }
                    else if (uobj.validUser.ToLower() == "f" && uobj.signeddate.AddDays(2).Date >= DateTime.Now.Date && uobj.password == model.password && uobj.username.ToUpper() == model.username.ToUpper())
                    {

                        ModelState.AddModelError("", "Please wait till admin provides approval.");
                        return View("Login");
                    }
                    else if (uobj.validUser.ToLower() == "r" && uobj.password == model.password && uobj.username.ToUpper() == model.username.ToUpper())

                    {
                        ModelState.AddModelError("", "Sorry! Your application has been cancelled by admin.");
                        return View("Login");
                    }
                    else
                    {
                        ModelState.AddModelError("", "The user name or password provided is incorrect.");
                    }

                    //using (userDbEntities entities = new userDbEntities())
                    //{
                    //    string username = model.username;
                    //    string password = model.password;
                    //    string phNum = model.phonenumber;

                    // Now if our password was enctypted or hashed we would have done the
                    // same operation on the user entered password here, But for now
                    // since the password is in plain text lets just authenticate directly

                    //bool userValid = entities.Users.Any(user => user.username == username && user.password == password);

                    // User found in the database
                    if (userValid)
                    {

                        FormsAuthentication.SetAuthCookie(uobj.username, false);
                        if (ReturnUrl != null && ReturnUrl != "" && ReturnUrl.Contains("currentAffairs"))
                        {
                            return RedirectToAction("Index", "currentAffairs", new { mode = "currentAffairs" });
                        }
                        else if (ReturnUrl != null && ReturnUrl != "" && ReturnUrl.Contains("currentAffairs"))

                        {
                            return RedirectToAction("Index", "currentAffairs", new { mode = "qusPaper" });
                        }

                        else
                        {
                            return RedirectToAction("Index", "Home");
                        }
                    }
                }
                catch (Exception)
                {
                    return View("Error");
                }
                    
                
            }

            // If we got this far, something failed, redisplay form
            return View("Login");
        }
        #endregion
        #region use logout
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            Session["UserID"] = null;
            Session["role"] = null;
            return RedirectToAction("Index", "Home");
        }
        #endregion
    }
}