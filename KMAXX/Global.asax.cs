﻿using KMAXX.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace KMAXX
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public static int totalcount = 0;
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            MvcHandler.DisableMvcResponseHeader = true;
            totalcount= numberofusers();
            updateusers(totalcount+1);
            Application["Totaluser"] = totalcount+1;
            
        }
        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            if (FormsAuthentication.CookiesSupported == true)
            {
                if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
                {
                    try
                    {
                        //let us take out the username now                
                        string username = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name;
                        string roles = string.Empty;
                        DataSet ds = new DataSet();
                        Users uobj = new Users();
                        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["kmaxxConString"].ConnectionString))
                        {
                            using (SqlCommand cmd = new SqlCommand("select * from users where username = @username", con))
                            {
                                cmd.CommandType = CommandType.Text;
                                cmd.Parameters.AddWithValue("@username", username);
                                con.Open();

                                SqlDataAdapter da = new SqlDataAdapter(cmd);
                                da.Fill(ds);

                                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                                {

                                    roles = ds.Tables[0].Rows[i]["roles"].ToString();
                                   // uobj.password = ds.Tables[0].Rows[i]["password"].ToString();
                                    // uobj.testDate = Convert.ToDateTime(ds.Tables[0].Rows[i]["date"]);                           
                                }
                            }
                            con.Close();
                        }
                        //let us extract the roles from our own custom cookie


                        //Let us set the Pricipal with our user specific details
                        HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(
                          new System.Security.Principal.GenericIdentity(username, "Forms"), roles.Split(';'));
                    }
                    catch (Exception)
                    {
                        //somehting went wrong
                    }
                }
            }
        }
        protected void Application_PreSendRequestHeaders()
        {
            Response.Headers.Remove("Server");
            Response.Headers.Remove("X-AspNet-Version");
        }
        protected void Session_Start()
        {
            Application.Lock();
           
            DataSet ds = new DataSet();
           
            Application.UnLock();
        }
        public bool updateusers(int count)
        {
            try
            {

                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["kmaxxConString"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("update noofusers set count = @count where id=1", con))
                    {
                        con.Open();
                        cmd.Parameters.Add("@count", SqlDbType.VarChar).Value = count;

                        cmd.ExecuteNonQuery();
                        con.Close();
                        return true;
                    }

                }
            }
            catch (Exception) {

                return false;
            }


        }
        public int numberofusers()
        {
            int totalcount = 0;
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["kmaxxConString"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("select * from noofusers", con))
                    {
                        cmd.CommandType = CommandType.Text;

                        con.Open();

                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(ds);

                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {

                            totalcount = Convert.ToInt32(ds.Tables[0].Rows[i]["count"]);
                            // uobj.password = ds.Tables[0].Rows[i]["password"].ToString();
                            // uobj.testDate = Convert.ToDateTime(ds.Tables[0].Rows[i]["date"]);                           
                        }
                    }
                    con.Close();

                }
            }
            catch (Exception) { }
            return totalcount;
        }
    }
}
